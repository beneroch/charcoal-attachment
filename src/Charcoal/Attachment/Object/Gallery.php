<?php

namespace Charcoal\Attachment\Object;

// From 'beneroch/charcoal-attachments'
use Charcoal\Attachment\Object\Container;

/**
 * Gallery Attachment Type
 *
 * This type allows for nesting of additional attachment types.
 */
class Gallery extends Container
{
}
